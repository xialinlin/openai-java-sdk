---
title: Release Notes

hide:
  - navigation
---

### 1.2.0

---

- Support report error message
- Supports custom `OkHttpClient`
- Add a default interceptor
- Support list api keys
- Support create new api key

### 1.1.0

---

- Fixed `DefaultClient`

### 1.0.0

---

- Support models
    - List models
    - Retrieve model
- Create completion
- Create chat completion
