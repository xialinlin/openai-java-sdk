package org.devlive.sdk.openai.model;

public enum UrlModel
{
    FETCH_MODELS,
    FETCH_MODEL,
    FETCH_COMPLETIONS,
    FETCH_CHAT_COMPLETIONS,
    FETCH_USER_API_KEYS,
    FETCH_CREATE_USER_API_KEY
}
